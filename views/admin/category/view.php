<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = "Show {$category->title}";
?>
<a href="<?= Url::to(['admin/category']) ?>">List Category</a>
<h3><?= Html::encode($this->title) ?></h3>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($category, 'title') ?>

    <?= $form->field($category, 'description') ?>

    <img src="<?= $category->getImage(); ?>" style="width:200px;" >
<?php ActiveForm::end(); ?>