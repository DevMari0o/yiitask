<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = "Edit {$category->title}";
?>
<a href="<?= Url::to(['admin/category']) ?>">List Category</a>
<h3><?= Html::encode($this->title) ?></h3>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($category, 'title') ?>

    <?= $form->field($category, 'description') ?>
    
    <?= $form->field($category, 'image')->fileInput() ?>

    <img src="<?= $category->getImage(); ?>" style="width:200px;" >
    <br><br>	
    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>