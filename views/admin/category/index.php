<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'All Category';
?>

<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

   
	<a href="<?= Url::to(['admin/category/create']) ?>" class="btn btn-primary">Add</a>
	<br><br>

	<?php if (Yii::$app->session->hasFlash('erorr')): ?>
        <div class="alert alert-danger">
            <?= Yii::$app->session->getFlash('erorr') ?>
        </div>
   	<?php endif;?>

    <table class="table">
    	<tr>
    		<td>#</td>
    		<td>Title</td>
    		<td>Option</td>
    	</tr>
    	<?php if(count($categories) > 0):?>
    	<?php $i =1; foreach($categories as $category):?>
    	<tr>
    		<td><?= $i++?></td>
    		<td><?= $category->title ?></td>
    		<td>
    			<a href="<?= Url::to(['admin/category/view', 'id' => $category->id]) ?>" class="btn btn-primary">Show</a> -
    			<a href="<?= Url::to(['admin/category/edit', 'id' => $category->id]) ?>" class="btn btn-primary">Edit</a> -
				<?php $form = ActiveForm::begin(['options' => ['style'=>'display:inline'],'action'=>'?r=admin/category/destory&id='.$category->id]); ?>
    				<?= Html::submitButton('Delete', ['class' => 'btn btn-primary']) ?>
				<?php ActiveForm::end(); ?>
    		</td>
    	</tr>
    	<?php endforeach;?>
    	<?php else:?>
    		<tr>
    			<td colspan="3">No Records Found</td>
    		</tr>
    	<?php endif;?>
    </table>
</div>
