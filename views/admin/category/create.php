<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php if (Yii::$app->session->hasFlash('erorr')): ?>
        <div class="alert alert-danger">
            <?= Yii::$app->session->getFlash('erorr') ?>
        </div>
<?php endif;?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($category, 'title') ?>

    <?= $form->field($category, 'description') ?>

    <?= $form->field($category, 'image')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>