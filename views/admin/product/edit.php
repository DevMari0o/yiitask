<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = "Show {$product->title}";
?>
<a href="<?= Url::to(['admin/product']) ?>">List Product</a>
<h3><?= Html::encode($this->title) ?></h3>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($product, 'title') ?>

    <?= $form->field($product, 'description') ?>
    
    <?= $form->field($product, 'image')->fileInput() ?>

    <label>Category</label>
    <select name="category_id" class="form-control">
    	<?php foreach($categories as $category):?>
    		<option value="<?= $category->id?>" <?= ($category->id == $product->category_id) ? 'selected' :'' ;?> > <?= $category->title?></option>
    	<?php endforeach; ?>
    </select>
    <br><br>	
    <img src="<?= $product->getImage(); ?>" style="width:200px;" >
    <br><br>	
    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>