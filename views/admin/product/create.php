<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php if (Yii::$app->session->hasFlash('erorr')): ?>
        <div class="alert alert-danger">
            <?= Yii::$app->session->getFlash('erorr') ?>
        </div>
<?php endif;?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($product, 'title') ?>

    <?= $form->field($product, 'description') ?>

    <label>Category</label>
    <select name="category_id" class="form-control">
    	<?php foreach($categories as $category):?>
    		<option value="<?= $category->id?>"><?= $category->title?></option>
    	<?php endforeach; ?>
    </select>
    
    <?= $form->field($product, 'image')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>