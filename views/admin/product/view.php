<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = "Show {$product->title}";
?>
<a href="<?= Url::to(['admin/product']) ?>">List Product</a>
<h3><?= Html::encode($this->title) ?></h3>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($product, 'title') ?>

    <?= $form->field($product, 'description') ?>
    <label>Category: </label><span><?= $product->category->title; ?></span>
    <br><br>
    <img src="<?= $product->getImage(); ?>" style="width:200px;" >
<?php ActiveForm::end(); ?>