<?php
namespace app\models\admin;

use yii\base\Model;
use yii\db\ActiveRecord;

class Category extends ActiveRecord
{

    public static function tableName()
    {
        return '{{categories}}';
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
        ];
    }

    public function rules()
	{
	    return [
	        [['title', 'description'], 'required'],
	        ['title','unique'],
	        [['title'],'string','min'=>5],
	        [['description'], 'string','min'=>20],
	        ['image','file', 'extensions' => ['png', 'jpg', 'gif']],

	    ];
	}

    public function getImage()
    {
        return 'images/category/'.$this->image;
    }

    public function beforeDelete()
	{
	    if (parent::beforeDelete()) {
	    	if (is_file($this->getImage())) {
	        	return unlink($this->getImage());
	    	}
	    }
	    return false;
	}

	public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }
}