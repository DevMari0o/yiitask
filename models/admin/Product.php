<?php
namespace app\models\admin;

use yii\base\Model;
use yii\db\ActiveRecord;

class Product extends ActiveRecord
{

    public static function tableName()
    {
        return '{{products}}';
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
            'category_id' => 'Category',
        ];
    }

    public function rules()
	{
	    return [
	        [['title', 'description'], 'required'],
	        ['title','unique'],
	        [['title'],'string','min'=>5],
	        [['description'], 'string','min'=>20],
	        ['image','file', 'extensions' => ['png', 'jpg', 'gif']],

	    ];
	}

    public function getImage()
    {
        return 'images/product/'.$this->image;
    }

    public function beforeDelete()
	{
	    if (parent::beforeDelete()) {
	    	if (is_file($this->getImage())) {
	        	return unlink($this->getImage());
	    	}
	    }
	    return true;
	}

	public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}