<?php

namespace app\controllers\admin;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use app\models\admin\Category;

class CategoryController extends Controller
{
   	// list all categories
   	public function actionIndex()
    {
        $categories = Category::find()->select(['id','title'])->all();;
    	return $this->render('index',['categories'=>$categories]);
    }

    // show create category form
    public function actionCreate()
    {
    	$request = Yii::$app->request->post('Category');
        $category = new Category();
        if (Yii::$app->request->isPost) {
        	return $this->store($request, $category);
        }else{
        	return $this->render('create', ['category' => $category]);
        }
    }

    private function store($request,Category $category)
    {
    	$category->load(Yii::$app->request->post());
		if ($category->validate()) {
			if ($this->uploadImage($category)) {
            	Yii::$app->session->setFlash('success','Category Added Successfully');
				return $this->redirect(['admin/category/view','id'=>$category->id]);
			}else{
				Yii::$app->session->setFlash('erorr','Can\'t Add Category Check Image');
			}
		}
    	return $this->render('create', ['category' => $category]);
		
    }

    // show single category
    public function actionView($id)
    {
        $category = Category::findOne($id);
        if ($category === null)  throw new NotFoundHttpException;
        return $this->render('view', [
            'category' => $category,
        ]);
    }

    // edit category
    public function actionEdit($id)
    {
        $category = Category::findOne($id);
        if ($category === null)  throw new NotFoundHttpException;
        if (Yii::$app->request->isPost) {
            $request = Yii::$app->request->post('Category');
            $category->title = $request['title'];
            $category->description = $request['description'];
    		if (!$this->uploadImage($category)) {
                $category->image = $category->image;
            }
            $category->save();
            Yii::$app->session->setFlash('success','Category Updated Successfully');
			return $this->redirect(['admin/category/edit','id'=>$category->id]);
        }
        return $this->render('edit', ['category' => $category]);
    }

    // delete category
    public function actionDestory($id)
    {
        if (Yii::$app->request->isPost) {
        	$category = Category::findOne($id);
	        if ($category === null)  throw new NotFoundHttpException;
	        if ($category->delete()) {
            	Yii::$app->session->setFlash('success','Category Deleted Successfully');
	        }else{
            	Yii::$app->session->setFlash('erorr',' Can\'t Delete Category');
	        }
        }
		return $this->redirect(['admin/category']);
    }

    public function uploadImage($category)
    {
    	if (UploadedFile::getInstance($category,'image')) {
            $category->image = UploadedFile::getInstance($category,'image');
			$imageName = rand(1,987699).'-'.$category->image->baseName.'.'.$category->image->extension;
		    $category->image->saveAs( 'images/category/'. $imageName);
		    $category->image = $imageName;
		    return $category->save();
		}
        return false;
    }
}
