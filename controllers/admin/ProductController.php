<?php

namespace app\controllers\admin;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use app\models\admin\Category;
use app\models\admin\Product;
use yii\data\Pagination;
class ProductController extends Controller
{
   	// list all products
   	public function actionIndex()
    {
        $query = Product::find()->select(['id','title']);
        $pages = new Pagination(['totalCount' => $query->count()]);
        $pages->defaultPageSize = 10;
        $models = $query->offset($pages->offset)->limit($pages->limit)->all();
        return $this->render('index', compact('pages','models'));
    }

    // show create product form
    public function actionCreate()
    {
    	$request = Yii::$app->request->post();
        $categories = Category::find()->select(['id','title'])->all();
        $product = new Product();
        if (Yii::$app->request->isPost) {
        	return $this->store($request, $product);
        }else{
        	return $this->render('create', compact('product','categories'));
        }
    }

    private function store($request,Product $product)
    {
        $categories = Category::find()->select(['id','title'])->all();
        $product->title = $request['Product']['title'];
        $product->category_id = $request['category_id'];
        $product->description = $request['Product']['description'];
        $product->created_at = $request['Product']['description'];
        $product->created_at = date('Y-m-d');
        if ($this->uploadImage($product)) {
        	Yii::$app->session->setFlash('success','Product Added Successfully');
			return $this->redirect(['admin/product/view','id'=>$product->id]);
		}
        Yii::$app->session->setFlash('erorr','Can\'t Add Product Check Image');
    	return $this->render('create', ['product' => $product,'categories'=>$categories]);
    }

    // show single product
    public function actionView($id)
    {
        $product = Product::findOne($id);
        if ($product === null)  throw new NotFoundHttpException;
        return $this->render('view', ['product' => $product]);
    }

    // edit product
    public function actionEdit($id)
    {
        $product = Product::findOne($id);
        $categories = Category::find()->select(['id','title'])->all();
        $request = Yii::$app->request->post();
        if (Yii::$app->request->isPost) {
            $this->update($request,$product);
        }
        return $this->render('edit', ['product' => $product,'categories'=>$categories]);
    }

    public function update($request,Product $product)
    {
        if ($product === null)  throw new NotFoundHttpException;
        $product->title = $request['Product']['title'];
        $product->category_id = $request['category_id'];
        $product->description = $request['Product']['description'];
        if(!$this->uploadImage($product)){
            $product->image = $product->image;
        }
        if ($product->save()) {
            return $this->redirect(['admin/product/edit','id'=>$product->id]);
        }
    }

    // delete product
    public function actionDestory($id)
    {
        if (Yii::$app->request->isPost) {
        	$product = Product::findOne($id);
	        if ($product === null)  throw new NotFoundHttpException;
	        if ($product->delete()) {
            	Yii::$app->session->setFlash('success','Product Deleted Successfully');
	        }else{
            	Yii::$app->session->setFlash('erorr',' Can\'t Delete Product');
	        }
        }
		return $this->redirect(['admin/product']);
    }

    public function uploadImage($product)
    {
        if (UploadedFile::getInstance($product,'image')) {
            $product->image = UploadedFile::getInstance($product,'image');
            $imageName = rand(1,987699).'-'.$product->image->baseName.'.'.$product->image->extension;
            $product->image->saveAs( 'images/product/'. $imageName);
            $product->image = $imageName;
            return $product->save();
        }
        return false;
    }
}
