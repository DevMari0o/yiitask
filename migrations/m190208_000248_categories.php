<?php

use yii\db\Migration;

/**
 * Class m190208_000248_categories
 */
class m190208_000248_categories extends Migration
{
    
    public function up()
    {
        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50)->notNull()->unique(),
            'description' => $this->text()->notNull(),
            'image' => $this->string()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('categories');
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190208_000248_categories cannot be reverted.\n";

        return false;
    }
    
}
