<?php

use yii\db\Migration;

/**
 * Class m190208_001048_products
 */
class m190208_001048_products extends Migration
{

    public function up()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'title' => $this->string(150)->notNull()->unique(),
            'description' => $this->text()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'image' => $this->string()->notNull(),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'id-products-category_id',
            'products',
            'category_id'
        );

        $this->addForeignKey(
            'fk-products-category_id',
            'products',
            'category_id',
            'categories',
            'id',
            'set null'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-products-category_id',
            'products'
        );

        $this->dropIndex(
            'id-products-category_id',
            'products'
        );

        $this->dropTable('products');
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190208_001048_products cannot be reverted.\n";

        return false;
    }

}
